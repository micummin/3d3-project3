from cgi import print_directory
from peer import *
import time

def main():

    """
    Add cars and peers to network
    """
    quit = False
    peers = []

    # Let the simulation know which vehicles you want to
    # instantiate on each road (1 -> 9)
    while not quit:
        vehicle = input('Are you on a car or a bike? [c/b] ')
        road = input('Are you on road 1 or road 2? [between 1 and 9] ')
        direction=input('What direction are you heading in?[N W S E] ')
        peers.append(Peer(vehicle=vehicle, road=road,direction=direction)) 

        ans = input('Would you like to add another peer? [y/n]:  ')
        if ans == 'n':
            quit = True
        elif ans != 'y':
            raise ValueError('You didnt say yes or no')
    
    """
    Exchange messages
    """
    # initialise sockets, have to use seperate thread for each car 
    # since we're simulating multicasting on a laptop
    for i in range(0, len(peers)):
        if peers[i].vehicle == 'c':
            peers[i].initCarSocket()
            peers[i].start()
        else:
            peers[i].initBikeSocket()
    
    # Start Simulation
    count = 0
    done = False
    while not done:
        # all bikes send multicast message to group
        for i in range(0, len(peers)):
            if peers[i].vehicle == 'b':
                peers[i].send()
                peers[i].initBikeSocket()

        # send messages every second
        count = count + 1
        time.sleep(2)

        if count == 2:
            print('simulation done -> closing sockets')
            for i in range(0,len(peers)):
                if peers[i].vehicle == 'c':
                    # join threads and close sockets
                    peers[i].close()
                    peers[i].join()

            done = True

        # Move bikes 
        elif count == 5:
            print("\nAll bikes moving to one road over")
            for i in range(0, len(peers)):
                if peers[i].vehicle == 'b':
                    # close sockets and intialise new ones
                    peers[i].close()
                    peers[i].update()

        
        print('\n')
        for i in range(0,len(peers)):
            peers[i].print_dir()
    
if __name__ == "__main__":
    main() 