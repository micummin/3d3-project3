#from multiprocessing.sharedctypes import Value
import socket
import struct
import threading
import select
import ctypes

class Peer(threading.Thread):

    def __init__(self, vehicle, road, direction, rating):
        # Initialise threading
        super(Peer, self).__init__()
        self.vehicle = vehicle
        self.road = int(road)
        self.direction=direction

        self.personal_rating_dir = {
                        'safety': rating[0], # rating from 1 to 10 - potholes, bad turns etc.
                        'cycle lane': rating[1], # binary: 1 -> cycle lane, 0 -> no cycle lane 
                        'busy': rating[2], # rating from 1 to 10 - busy road, cars parked in cycle lane etc.    
                    }

        if self.road < 1 or self.road > 9:
            raise ValueError('Enter a valid road number')

        # give port numbers and ip based on road
        self.ip = '224.1.1.' + road
        if self.road < 10:
            self.port = int('500' + road)
        elif self.road < 100:
            self.port = int('50' + road)
        elif self.road < 1000:
            self.port = int('5' + road)

        # Set up individual port numbers and sockets depending on vehicle   
        if self.vehicle == 'c':
            self.initCarSocket()
        elif self.vehicle == 'b':
            self.initBikeSocket()
        else:
            raise ValueError("vehicle must be car or bike")

    def update(self):
        # increment port number by 1 since we're moving one road over
        self.port = self.port + 1
        self.ip = self.ip[:-1] + str(int(self.road) + 1)
        # re-initialise a socket with the correct ip and port number 
        self.initBikeSocket()
    
    def decodeMsg(self, msg):
        pass

    def updatePersonalRating(self, rating):
        self.personal_rating_dir = {
                        'safety': rating[0], # rating from 1 to 10 - potholes, bad turns etc.
                        'cycle lane': rating[1], # binary: 1 -> cycle lane, 0 -> no cycle lane 
                        'busy': rating[2], # rating from 1 to 10 - busy road, cars parked in cycle lane etc.    
                        }

    def initCarSocket(self):
        if self.vehicle != "c":
            raise ValueError("Can't initialise car socket, change vehicle type")
        # intialise UDP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # use the group multicast port number and ip as data source
        self.sock.bind((self.ip, self.port))
        # Add receiver to multicast group
        mreq = struct.pack("4sl", socket.inet_aton(self.ip), socket.INADDR_ANY)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    
    def initBikeSocket(self):
        if self.vehicle != "b":
            raise ValueError("Can't initialise bike socket, change vehicle type")
        # Initialise UDP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.sock.settimeout(0.2)
        # set as multicast sender
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)
    
    def send(self):
        if self.vehicle == "c":
            raise ValueError("cars can't send messages")
        # convert message to bytes before sending
        msg = f"msg from bike on port {self.port}"
        msg = bytes(msg, 'utf-8')
        # send message to multicast group
        self.sock.sendto(msg, (self.ip, self.port))
        print(f'Bike {self.port}: sending messages to cars with port {self.port} ')
       # print (f'Bike heading in direction {self.direction} ')

    def print_dir(self):
        print(f'{self.vehicle} {self.port} is travelling in direction: {self.direction}')
          
    def run(self):
        if self.vehicle == 'c':
            done = False
            while not done:
                # identify which car is listening and which bike it recieved a message from
                # first, check if we're recieving a message
                # wait one second to check if car                     
                try:
                    rcv, _, _ = select.select([self.sock], [], [], 2.05)
                    if rcv:
                        rcv_msg = self.sock.recv(4092)
                        print(f'Car {self.port}: Bike on road -> {rcv_msg}')
                    else:
                        print(f'Car {self.port}: no bike on road')
                except:
                    print(f'thread {self.get_id()} with peer {self.vehicle} {self.port} ended')
                    done = True
    
    """
    Utility functions for closing threads and sockets
    """
    def get_id(self):
        # returns id of the respective thread
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def close(self):
        # First close socket 
        self.sock.close()
        print(f'peer: {self.port} closed')
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
              ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')
